# * Returns a version string from Git
#
# These functions force a re-configure on each git commit so that you can trust the values of the variables in your
# build system.
#
# get_git_head_revision(<refspecvar> <hashvar> [ALLOW_LOOKING_ABOVE_CMAKE_SOURCE_DIR])
#
# Returns the refspec and sha hash of the current head revision
#
# git_describe(<var> [<additional arguments to git describe> ...])
#
# Returns the results of git describe on the source tree, and adjusting the output so that it tests false if an error
# occurs.
#
# git_describe_working_tree(<var> [<additional arguments to git describe> ...])
#
# Returns the results of git describe on the working tree (--dirty option), and adjusting the output so that it tests
# false if an error occurs.
#
# git_get_exact_tag(<var> [<additional arguments to git describe> ...])
#
# Returns the results of git describe --exact-match on the source tree, and adjusting the output so that it tests false
# if there was no exact matching tag.
#
# git_local_changes(<var>)
#
# Returns either "CLEAN" or "DIRTY" with respect to uncommitted changes. Uses the return code of "git diff-index --quiet
# HEAD --". Does not regard untracked files.
#
# Requires CMake 2.6 or newer (uses the 'function' command)
#
# Original Author: 2009-2020 Ryan Pavlik <ryan.pavlik@gmail.com> <abiryan@ryand.net> http://academic.cleardefinition.com
#
# Copyright 2009-2013, Iowa State University. Copyright 2013-2020, Ryan Pavlik Copyright 2013-2020, Contributors
# SPDX-License-Identifier: BSL-1.0 Distributed under the Boost Software License, Version 1.0. (See accompanying file
# LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

IF(__get_git_revision_description)
	RETURN()
ENDIF()
SET(__get_git_revision_description YES)

# We must run the following at "include" time, not at function call time, to find the path to this module rather than
# the path to a calling list file
GET_FILENAME_COMPONENT(_gitdescmoddir ${CMAKE_CURRENT_LIST_FILE} PATH)

# Function _git_find_closest_git_dir finds the next closest .git directory that is part of any directory in the path
# defined by _start_dir. The result is returned in the parent scope variable whose name is passed as variable
# _git_dir_var. If no .git directory can be found, the function returns an empty string via _git_dir_var.
#
# Example: Given a path C:/bla/foo/bar and assuming C:/bla/.git exists and neither foo nor bar contain a file/directory
# .git. This wil return C:/bla/.git
#
FUNCTION(_git_find_closest_git_dir _start_dir _git_dir_var)
	SET(cur_dir "${_start_dir}")
	SET(git_dir "${_start_dir}/.git")
	WHILE(NOT EXISTS "${git_dir}")
		# .git dir not found, search parent directories
		SET(git_previous_parent "${cur_dir}")
		GET_FILENAME_COMPONENT(cur_dir "${cur_dir}" DIRECTORY)
		IF(cur_dir STREQUAL git_previous_parent)
			# We have reached the root directory, we are not in git
			SET(${_git_dir_var} "" PARENT_SCOPE)
			RETURN()
		ENDIF()
		SET(git_dir "${cur_dir}/.git")
	ENDWHILE()
	SET(${_git_dir_var} "${git_dir}" PARENT_SCOPE)
ENDFUNCTION()

FUNCTION(get_git_head_revision _refspecvar _hashvar)
	_GIT_FIND_CLOSEST_GIT_DIR("${CMAKE_CURRENT_SOURCE_DIR}" GIT_DIR)

	IF("${ARGN}" STREQUAL "ALLOW_LOOKING_ABOVE_CMAKE_SOURCE_DIR")
		SET(ALLOW_LOOKING_ABOVE_CMAKE_SOURCE_DIR TRUE)
	ELSE()
		SET(ALLOW_LOOKING_ABOVE_CMAKE_SOURCE_DIR FALSE)
	ENDIF()
	IF(NOT
	   "${GIT_DIR}"
	   STREQUAL
	   "")
		FILE(
			RELATIVE_PATH
			_relative_to_source_dir
			"${CMAKE_SOURCE_DIR}"
			"${GIT_DIR}")
		IF("${_relative_to_source_dir}" MATCHES "[.][.]" AND NOT ALLOW_LOOKING_ABOVE_CMAKE_SOURCE_DIR)
			# We've gone above the CMake root dir.
			SET(GIT_DIR "")
		ENDIF()
	ENDIF()
	IF("${GIT_DIR}" STREQUAL "")
		SET(${_refspecvar} "GITDIR-NOTFOUND" PARENT_SCOPE)
		SET(${_hashvar} "GITDIR-NOTFOUND" PARENT_SCOPE)
		RETURN()
	ENDIF()

	# Check if the current source dir is a git submodule or a worktree. In both cases .git is a file instead of a
	# directory.
	#
	IF(NOT IS_DIRECTORY ${GIT_DIR})
		# The following git command will return a non empty string that points to the super project working tree if the
		# current source dir is inside a git submodule. Otherwise the command will return an empty string.
		#
		EXECUTE_PROCESS(
			COMMAND "${GIT_EXECUTABLE}" rev-parse --show-superproject-working-tree
			WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
			OUTPUT_VARIABLE out
			ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
		IF(NOT
		   "${out}"
		   STREQUAL
		   "")
			# If out is empty, GIT_DIR/CMAKE_CURRENT_SOURCE_DIR is in a submodule
			FILE(READ ${GIT_DIR} submodule)
			STRING(
				REGEX
				REPLACE "gitdir: (.*)$"
						"\\1"
						GIT_DIR_RELATIVE
						${submodule})
			STRING(STRIP ${GIT_DIR_RELATIVE} GIT_DIR_RELATIVE)
			GET_FILENAME_COMPONENT(SUBMODULE_DIR ${GIT_DIR} PATH)
			GET_FILENAME_COMPONENT(GIT_DIR ${SUBMODULE_DIR}/${GIT_DIR_RELATIVE} ABSOLUTE)
			SET(HEAD_SOURCE_FILE "${GIT_DIR}/HEAD")
		ELSE()
			# GIT_DIR/CMAKE_CURRENT_SOURCE_DIR is in a worktree
			FILE(READ ${GIT_DIR} worktree_ref)
			# The .git directory contains a path to the worktree information directory inside the parent git repo of the
			# worktree.
			#
			STRING(
				REGEX
				REPLACE "gitdir: (.*)$"
						"\\1"
						git_worktree_dir
						${worktree_ref})
			STRING(STRIP ${git_worktree_dir} git_worktree_dir)
			_GIT_FIND_CLOSEST_GIT_DIR("${git_worktree_dir}" GIT_DIR)
			SET(HEAD_SOURCE_FILE "${git_worktree_dir}/HEAD")
		ENDIF()
	ELSE()
		SET(HEAD_SOURCE_FILE "${GIT_DIR}/HEAD")
	ENDIF()
	SET(GIT_DATA "${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/git-data")
	IF(NOT EXISTS "${GIT_DATA}")
		FILE(MAKE_DIRECTORY "${GIT_DATA}")
	ENDIF()

	IF(NOT EXISTS "${HEAD_SOURCE_FILE}")
		RETURN()
	ENDIF()
	SET(HEAD_FILE "${GIT_DATA}/HEAD")
	CONFIGURE_FILE("${HEAD_SOURCE_FILE}" "${HEAD_FILE}" COPYONLY)

	CONFIGURE_FILE("${_gitdescmoddir}/GetGitRevisionDescription.cmake.in" "${GIT_DATA}/grabRef.cmake" @ONLY)
	INCLUDE("${GIT_DATA}/grabRef.cmake")

	SET(${_refspecvar} "${HEAD_REF}" PARENT_SCOPE)
	SET(${_hashvar} "${HEAD_HASH}" PARENT_SCOPE)
ENDFUNCTION()

FUNCTION(git_describe _var)
	IF(NOT GIT_FOUND)
		FIND_PACKAGE(Git QUIET)
	ENDIF()
	GET_GIT_HEAD_REVISION(refspec hash)
	IF(NOT GIT_FOUND)
		SET(${_var} "GIT-NOTFOUND" PARENT_SCOPE)
		RETURN()
	ENDIF()
	IF(NOT hash)
		SET(${_var} "HEAD-HASH-NOTFOUND" PARENT_SCOPE)
		RETURN()
	ENDIF()

	# TODO sanitize if((${ARGN}" MATCHES "&&") OR (ARGN MATCHES "||") OR (ARGN MATCHES "\\;")) message("Please report
	# the following error to the project!") message(FATAL_ERROR "Looks like someone's doing something nefarious with
	# git_describe! Passed arguments ${ARGN}") endif()

	# message(STATUS "Arguments to execute_process: ${ARGN}")

	EXECUTE_PROCESS(
		COMMAND "${GIT_EXECUTABLE}" describe --tags --always ${hash} ${ARGN}
		WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
		RESULT_VARIABLE res
		OUTPUT_VARIABLE out
		ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
	IF(NOT
	   res
	   EQUAL
	   0)
		SET(out "${out}-${res}-NOTFOUND")
	ENDIF()

	SET(${_var} "${out}" PARENT_SCOPE)
ENDFUNCTION()

FUNCTION(git_describe_working_tree _var)
	IF(NOT GIT_FOUND)
		FIND_PACKAGE(Git QUIET)
	ENDIF()
	IF(NOT GIT_FOUND)
		SET(${_var} "GIT-NOTFOUND" PARENT_SCOPE)
		RETURN()
	ENDIF()

	EXECUTE_PROCESS(
		COMMAND "${GIT_EXECUTABLE}" describe --dirty ${ARGN}
		WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
		RESULT_VARIABLE res
		OUTPUT_VARIABLE out
		ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
	IF(NOT
	   res
	   EQUAL
	   0)
		SET(out "${out}-${res}-NOTFOUND")
	ENDIF()

	SET(${_var} "${out}" PARENT_SCOPE)
ENDFUNCTION()

FUNCTION(git_get_exact_tag _var)
	GIT_DESCRIBE(out --exact-match ${ARGN})
	SET(${_var} "${out}" PARENT_SCOPE)
ENDFUNCTION()

FUNCTION(git_local_changes _var)
	IF(NOT GIT_FOUND)
		FIND_PACKAGE(Git QUIET)
	ENDIF()
	GET_GIT_HEAD_REVISION(refspec hash)
	IF(NOT GIT_FOUND)
		SET(${_var} "GIT-NOTFOUND" PARENT_SCOPE)
		RETURN()
	ENDIF()
	IF(NOT hash)
		SET(${_var} "HEAD-HASH-NOTFOUND" PARENT_SCOPE)
		RETURN()
	ENDIF()

	EXECUTE_PROCESS(
		COMMAND "${GIT_EXECUTABLE}" diff-index --quiet HEAD --
		WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
		RESULT_VARIABLE res
		OUTPUT_VARIABLE out
		ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
	IF(res EQUAL 0)
		SET(${_var} "CLEAN" PARENT_SCOPE)
	ELSE()
		SET(${_var} "DIRTY" PARENT_SCOPE)
	ENDIF()
ENDFUNCTION()
