FUNCTION(print_cmake_variables)
	GET_CMAKE_PROPERTY(_variableNames VARIABLES)
	LIST(SORT _variableNames)
	FOREACH(_variableName ${_variableNames})
		IF(ARGV0)
			UNSET(MATCHED)
			STRING(
				REGEX MATCH
					  ${ARGV0}
					  MATCHED
					  ${_variableName})
			IF(NOT MATCHED)
				CONTINUE()
			ENDIF()
		ENDIF()
		MESSAGE(STATUS "${_variableName}=${${_variableName}}")
	ENDFOREACH()
ENDFUNCTION()
